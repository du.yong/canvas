/**
 * Created by dy on 2017/5/25.
 */
/*
 * 随机数
 * */
function getRandom(start,end,math) {
    var diff = end - start;
    if(math === 0){
        var index = Math.random()*diff+start;
    }else if(math === -1){
        var index = Math.floor(Math.random()*diff)+start;
    }else if(math === 1){
        var index = Math.ceil(Math.random()*diff)+start;
    }else if(math === '') {
        var index = Math.round(Math.random() * diff) + start;
    }
    return index;
}

/*
 * 已知两点坐标求长度
 * */
function dis(x1,y1,x2,y2) {
    return Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
}