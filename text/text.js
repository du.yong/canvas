/**
 * Created by dy on 2017/5/25.
 */
var WIDTH  = Math.min(800,$(window).width()-50);
var HEIGHT = WIDTH;
var canvas = $("#canvas");
var cxt = canvas[0].getContext('2d');
window.onload = function () {
    canvas[0].width = WIDTH;
    canvas[0].height = HEIGHT;
    drawgrid(cxt);
};

/*
* 绘制米格
* */
function drawgrid(cxt) {
    cxt.save();
    cxt.beginPath();
    cxt.fillStyle = 'white';
    cxt.fillRect(0,0,WIDTH,HEIGHT);
    cxt.restore();
    cxt.save();
    cxt.beginPath();
    cxt.lineWidth = 15;
    cxt.strokeStyle = "red";
    cxt.strokeRect(0,0,WIDTH,HEIGHT);
    cxt.restore();
    cxt.save();
    cxt.strokeStyle = "red";
    cxt.lineWidth = 1;
    drawline(cxt,0,HEIGHT/2,WIDTH,HEIGHT/2);
    drawline(cxt,WIDTH/2,0,WIDTH/2,HEIGHT);
    drawline(cxt,0,0,WIDTH,HEIGHT);
    drawline(cxt,WIDTH,0,0,HEIGHT);
    cxt.restore();
}

/*
* 绘制直线
* */
function drawline(cxt,x1,y1,x2,y2) {
    cxt.beginPath();
    cxt.moveTo(x1,y1);
    cxt.lineTo(x2,y2);
    cxt.stroke();
}

/*
* 计算鼠标相对于canvas的坐标
* */
function getCanvasPoint(x,y) {
    var box = canvas[0].getBoundingClientRect();
    return {x:x-box.left,y:y-box.top};
}
var isdown = 0;
canvas.mousedown(function (e) {
    e.preventDefault();
    startWrite(e.clientX,e.clientY);
});
canvas.mousemove(function (e) {
    e.preventDefault();
    write(e.clientX,e.clientY);
});
canvas.mouseup(function (e) {
    e.preventDefault();
    stopWrite();
});
canvas.mouseout(function (e) {
    e.preventDefault();
    stopWrite();
});
canvas[0].addEventListener('touchstart',function (e) {
    e.preventDefault();
    var touch = e.touches[0];
    startWrite(touch.pageX,touch.pageY);
});
canvas[0].addEventListener('touchmove',function (e) {
    e.preventDefault();
    var touch = e.touches[0];
    write(touch.pageX,touch.pageY);
});
canvas[0].addEventListener('touchend',function (e) {
    e.preventDefault();
    stopWrite();
});
var lastPoint = {x:0,y:0};
var lastTime = 0;
var lastWidth = 0;
function startWrite(x,y){
    isdown = 1;
    var canvasPoint= getCanvasPoint(x,y);
    lastPoint = canvasPoint;
    lastTime = new Date().getTime();
}

function write(x,y) {
    if(isdown){
        var curPoint = getCanvasPoint(x,y);
        var curTime = new Date().getTime();
        var lineWidth = getlineWidth(curPoint,curTime);
        cxt.save();
        cxt.strokeStyle = "black";
        cxt.lineWidth = lineWidth;
        cxt.lineCap = 'round';
        cxt.lineJoin = 'round';
        drawline(cxt,lastPoint.x,lastPoint.y,curPoint.x,curPoint.y);
        cxt.restore();
        lastPoint = curPoint;
        lastTime = curTime;
        lastWidth = lineWidth;
    }
}

function stopWrite() {
    isdown = 0;
}
var maxlineWidth = 20;
var minlineWidth = 1;
var maxV = 3;
var minV = 1;
function getlineWidth(point,time) {
    var s = getPointlong(point);
    var v = Math.ceil(s/(time - lastTime));
    if(v<minV){
        var lineWidth = maxlineWidth;
    }else if(v>maxV){
        var lineWidth = minlineWidth;
    }else {
        var lineWidth = maxlineWidth - (v - minV) / (maxV - minV) * (maxlineWidth - minlineWidth);
    }
    if(lastWidth == 0){
        return lineWidth;
    }else {
        lineWidth = lastWidth * 3 / 4 + lineWidth * (1 / 4);
    }
    return lineWidth;
}

function getPointlong(point) {
    return Math.sqrt((point.x-lastPoint.x)*(point.x-lastPoint.x)+(point.y-lastPoint.y)*(point.y-lastPoint.y));
}

$("#con #clear button").click(function () {
    cxt.clearRect(0,0,WIDTH,HEIGHT);
    drawgrid(cxt);
});
$("#con #save button").click(function (e) {
    var filename = prompt("请输入文件名");
    var base64Img = canvas[0].toDataURL('image/jpeg',0.5);
    if(!filename){
        return alert("文件名不能为空");
    }
    var url = "./upload.php";
    $.post(url,{data:base64Img,name:filename},function (res) {
        res = JSON.parse(res);
        if(res.code == 0){
            $("#download").attr("href","./download.php?filename="+res.data);
            $("#download")[0].click();
        }
    });
});