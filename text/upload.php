<?php
/**
 * Created by PhpStorm.
 * User: dy
 * Date: 2017/6/5
 * Time: 14:24
 */
if(empty($_POST)){
    die(-1);
}
$file = "./upload/{$_POST['name']}.jpg";
$img = str_replace('data:image/jpeg;base64,', '', $_POST['data']);
$img = str_replace(' ', '+', $img);
$res = file_put_contents( $file , base64_decode($img) );
if($res) {
    die(json_encode(array('code'=>0,'data'=>$file)));
}
die('error');