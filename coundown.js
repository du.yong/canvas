/**
 * Created by dy on 2017/5/20.
 */
var WINDOW_WIDTH=1278;//屏幕初始尺寸
var WINDOW_HEIGHT=768;
var R = 8;//小球半径
var MARGIN_LEFT = 30;//初始位置
var MARGIN_TOP = 60;//初始top位置
var offsetLeft = MARGIN_LEFT;//初始位置偏移量
var margin = 20;//数字直接的间隔
/*
* 倒计时修改
* */
var time = 3600*1000;
var end_time = new Date();
end_time.setTime(end_time.getTime()+time);
var HOURS = diffHours();//时钟调用query时间函数
var MIN = diffMin();
var SEC = diffSec();
var balls = [];//小球数组
var outLen = 500;//小球数量限制
window.onload = function () {
    WINDOW_WIDTH = document.body.clientWidth;
    WINDOW_HEIGHT = document.body.clientHeight;
    MARGIN_TOP = document.body.clientHeight/5;
    var canvas = document.getElementById("canvas");
    var content = canvas.getContext('2d');
    canvas.width = WINDOW_WIDTH;
    canvas.height = WINDOW_HEIGHT;
    setInterval(function () {
        if(!document.hidden) {//防止离开页面小球堆积
            render(content);
            update();
        }
    },20);
}
//主逻辑
/*
* 一个数字的宽度为:digit[0][0].length*2*(R+1)
* 一个冒号的宽度为: digit[10][0].length*2*(R+1)
* 每个数字冒号之间的间隔用margin
* */
function render(cxt) {
    cxt.clearRect(0,0,WINDOW_WIDTH,WINDOW_HEIGHT);
    /*
    * 屏幕自适应
    * */
    var num_H= digit[0][0].length*2*(R+1);
    var str_H = digit[10][0].length*2*(R+1);
    var total_H = num_H*7+str_H*2;
    MARGIN_LEFT = (document.body.clientWidth - total_H)/2;
    offsetLeft = MARGIN_LEFT;
    var hours = HOURS;
    var min = MIN;
    var sec = SEC;
    //绘制时钟
    var number = [hours/10,hours%10,min/10,min%10,sec/10,sec%10];
    for(var i = 0;i<number.length;i++){
        renderDigit(MARGIN_LEFT, MARGIN_TOP, Math.floor(number[i]), cxt);
        MARGIN_LEFT += digit[0][0].length*2*(R+1)+margin;
        if(i != number.length - 1 && i%2 != 0){
            renderDigit(MARGIN_LEFT,MARGIN_TOP,10,cxt);
            MARGIN_LEFT += digit[10][0].length*2*(R+1)+margin;
        }
    }
    //绘制小球
    renderBalls(cxt);
}
/*
* 绘制数字
* 通过格子系统计算坐标位置的公式
* centerX: x+j*2*(R+1)+(R+1)
* centerY: y+i*2*(R+1)+(R+1)
* R为小圆的半径,R+1为包裹小圆的格子半径 即格子的一半边长,之所以要加1是因为小圆之间要留有空隙
* x和y为初始绘制点的偏移量,不加x和y会从边缘开始绘制
* */
function renderDigit(x,y,num,cxt) {
    cxt.fillStyle = "rgb(0,102,153)";
    for(var i = 0;i<digit[num].length;i++){
        for(var j = 0;j<digit[num][i].length;j++){
            if(digit[num][i][j] == 1){
                cxt.beginPath();
                cxt.arc(x+j*2*(R+1)+(R+1),y+i*2*(R+1)+(R+1),R,0,2*Math.PI);
                cxt.fill();
            }
        }
    }
}

/*
* 绘制小球
* */
function renderBalls(cxt) {
    for(var i = 0;i<balls.length;i++){
        cxt.fillStyle = balls[i].color;
        cxt.beginPath();
        cxt.arc(balls[i].x,balls[i].y,R,0,2*Math.PI);
        cxt.fill();
    }
}

/*
* 数据更新
* */
function update() {
    var nextHours = diffHours();
    var nextMin = diffMin();
    var nextSec = diffSec();
    var num_H = digit[0][0].length*2*(R+1)+margin;
    var str_H = digit[10][0].length*2*(R+1)+margin;
    if(nextSec != SEC){
        if(Math.floor(SEC/10) != Math.floor(nextSec/10)){
            addBall(offsetLeft+num_H*4+str_H*2,MARGIN_TOP,Math.floor(nextSec/10));
        }
        if(Math.floor(SEC%10) != Math.floor(nextSec%10)){
            addBall(offsetLeft+num_H*5+str_H*2,MARGIN_TOP,Math.floor(nextSec%10));
        }
        SEC = nextSec;

    }
    if(nextMin != MIN){
        if(Math.floor(MIN/10) != Math.floor(nextMin/10)){
            addBall(offsetLeft+num_H*2+str_H,MARGIN_TOP,Math.floor(nextMin/10));
        }
        if(Math.floor(MIN%10) != Math.floor(nextMin%10)){
            addBall(offsetLeft+num_H*3+str_H,MARGIN_TOP,Math.floor(nextMin%10));

        }
        MIN = nextMin;
    }
    if(nextHours != HOURS){
        if(Math.floor(HOURS/10) != Math.floor(nextHours/10)){
            addBall(offsetLeft,MARGIN_TOP,Math.floor(nextHours/10));
        }
        if(Math.floor(HOURS%10) != Math.floor(nextHours%10)){
            addBall(offsetLeft+num_H,MARGIN_TOP,Math.floor(nextHours%10))
        }
        HOURS = nextHours;
    }
    updateBalls();
}

/*
 * 产生小球
 * */
function addBall(x,y,num) {
    for(var i = 0;i<digit[num].length;i++){
        for(var j = 0;j<digit[num][i].length;j++){
            if(digit[num][i][j] == 1){
                var oneBall = {
                    x:x+j*2*(R+1)+(R+1),
                    y:y+i*2*(R+1)+(R+1),
                    g:getRandom(1.5,2.5,0),
                    vx:getRandom(-5,2,0),
                    vy:getRandom(-10,-2,0),
                    fs:getRandom(0.6,0.8,0),
                    color:getColor()
                };
                balls.push(oneBall);
            }
        }
    }
    checkBalls();
}

/*
 * 小球运动
 * */
function updateBalls() {
    for(var i = 0;i<balls.length;i++){
        balls[i].x += balls[i].vx;
        balls[i].y += balls[i].vy;
        balls[i].vy += balls[i].g;
        if(balls[i].y >= WINDOW_HEIGHT-R){
            balls[i].y = WINDOW_HEIGHT-R;
            balls[i].vy = -balls[i].vy*balls[i].fs;
        }
    }
}

/*
* 防止小球数组过大机制
* */
function checkBalls() {
    if(balls.length > outLen){
        balls.splice(0,balls.length-outLen);
    }
}

function queryHours() {
    var datetime = new Date();
    var hours = datetime.getHours();
    return hours;
}

function queryMin() {
    var datetime = new Date();
    var min = datetime.getMinutes();
    return min;
}

function querySec() {
    var datetime = new Date();
    var sec = datetime.getSeconds();
    return sec;
}

/*
* 随机颜色
* */
function getColor() {
    var rgb = "rgb(";
    for(var i = 0;i<3;i++){
        if(i != 2) {
            rgb += getRandom(0,255,'') + ",";
        }else{
            rgb += getRandom(0,255,'') + ")";
        }
    }
    return rgb;
}

/*
* 随机数
* */
function getRandom(start,end,math) {
    var diff = end - start;
    if(math === 0){
        var index = Math.random()*diff+start;
    }else if(math === -1){
        var index = Math.floor(Math.random()*diff)+start;
    }else if(math === 1){
        var index = Math.ceil(Math.random()*diff)+start;
    }else if(math === '') {
        var index = Math.round(Math.random() * diff) + start;
    }
    return index;
}

/*
* 计算剩余时间小时
* */
function diffHours() {
    var datetime = new Date();
    var difftime = end_time.getTime();
    var nowtime = datetime.getTime();
    var hour = Math.floor((difftime - nowtime)/1000/60/60);
    return hour;
}

/*
* 计算时间剩余分钟
* */
function diffMin() {
    var datetime = new Date();
    var difftime = end_time.getTime();
    var nowtime = datetime.getTime();
    var min = Math.floor((difftime - nowtime)/(1000*60))%60;
    return min;
}

/*
* 计算时间剩余秒
* */
function diffSec() {
    var datetime = new Date();
    var difftime = end_time.getTime();
    var nowtime = datetime.getTime();
    var sec = Math.floor((difftime - nowtime)/1000)%60;
    return sec;
}
